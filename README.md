# jwt-auth-client

decode jwt tokens, and use it as a plain php object

Get [TokenDecoder](src/Services/TokenDecoder.php) and decode token, instance of
[AccessToken](src/Entities/AccessToken.php) will be returned, if something is wrong
an instance of [AuthClientException](src/Exceptions/AuthClientException.php) will be thrown
