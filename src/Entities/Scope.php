<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 7/20/17
 * Time: 11:25 AM
 */

namespace Skipper\JwtAuthClient\Entities;

class Scope
{
    /** @var string $domain */
    protected $domain;
    /** @var array $permissions */
    protected $permissions = [];

    /**
     * @return string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * @return array
     */
    public function getPermissions(): array
    {
        return $this->permissions;
    }

    /**
     * @param string $domain
     * @return $this
     */
    public function setDomain(string $domain): Scope
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * @param array $permissions
     * @return $this
     */
    public function setPermissions(array $permissions): Scope
    {
        $this->permissions = $permissions;
        return $this;
    }
}