<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 7/20/17
 * Time: 11:55 AM
 */

namespace Skipper\JwtAuthClient\Entities;

use Skipper\JwtAuthClient\Exceptions\AuthClientException;

class GrantType
{
    const CLIENT_CREDENTIALS = 'client_credentials';
    const USER_CREDENTIALS = 'user_credentials';
    /** @var string $type */
    protected $type;

    /**
     * GrantType constructor.
     * @param string $type
     * @throws AuthClientException
     */
    public function __construct(string $type)
    {
        if (false === in_array($type, [self::CLIENT_CREDENTIALS, self::USER_CREDENTIALS])) {
            throw new AuthClientException();
        }
        $this->type = $type;
    }

    /**
     * @return GrantType
     * @throws AuthClientException
     */
    public static function user(): GrantType
    {
        $gt = new static(self::USER_CREDENTIALS);
        return $gt;
    }

    /**
     * @return GrantType
     * @throws AuthClientException
     */
    public static function client(): GrantType
    {
        $gt = new static(self::CLIENT_CREDENTIALS);
        return $gt;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}