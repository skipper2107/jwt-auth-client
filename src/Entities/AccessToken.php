<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 7/20/17
 * Time: 11:15 AM
 */

namespace Skipper\JwtAuthClient\Entities;

class AccessToken
{
    /** @var int $userId */
    protected $userId;
    /** @var GrantType $grantType */
    protected $grantType;
    /** @var \DateTime $expireAt */
    protected $expireAt;
    /** @var \DateTime $issuedAt */
    protected $issuedAt;
    /** @var array $scopes */
    protected $scopes = [];
    /** @var string $tokenId */
    protected $tokenId;
    /** @var string $issuer */
    protected $issuer;

    /**
     * @return string
     */
    public function getIssuer(): string
    {
        return $this->issuer;
    }

    /**
     * @param string $issuer
     * @return $this
     */
    public function setIssuer(string $issuer): AccessToken
    {
        $this->issuer = $issuer;
        return $this;
    }

    /**
     * @return GrantType
     */
    public function getGrantType(): GrantType
    {
        return $this->grantType;
    }

    /**
     * @param GrantType $grantType
     * @return $this
     */
    public function setGrantType(GrantType $grantType): AccessToken
    {
        $this->grantType = $grantType;
        return $this;
    }

    /**
     * @param $domain string
     * @return bool
     */
    public function hasScope(string $domain): bool
    {
        $scopeAliases = array_keys($this->scopes);
        return in_array($domain, $scopeAliases);
    }

    /**
     * @return \DateTime
     */
    public function getExpireAt(): \DateTime
    {
        return $this->expireAt;
    }

    /**
     * @param \DateTime $expireAt
     * @return $this
     */
    public function setExpireAt(\DateTime $expireAt): AccessToken
    {
        $this->expireAt = $expireAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getIssuedAt(): \DateTime
    {
        return $this->issuedAt;
    }

    /**
     * @param \DateTime $issuedAt
     * @return $this
     */
    public function setIssuedAt(\DateTime $issuedAt): AccessToken
    {
        $this->issuedAt = $issuedAt;
        return $this;
    }

    /**
     * @return array
     */
    public function getScopes(): array
    {
        return $this->scopes;
    }

    /**
     * @param array $scopes
     * @return $this
     */
    public function setScopes(array $scopes): AccessToken
    {
        $this->scopes = $scopes;
        return $this;
    }

    /**
     * @return string
     */
    public function getTokenId(): string
    {
        return $this->tokenId;
    }

    /**
     * @param string $tokenId
     * @return $this
     */
    public function setTokenId(string $tokenId): AccessToken
    {
        $this->tokenId = $tokenId;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId(int $userId): AccessToken
    {
        $this->userId = $userId;
        return $this;
    }
}