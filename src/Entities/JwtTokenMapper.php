<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 30.07.17
 * Time: 10:20
 */

namespace Skipper\JwtAuthClient\Entities;

interface JwtTokenMapper
{
    const DEFAULT_DECODE_ALGORITHM = 'RS256';

    const USER_ID_PAYLOAD_KEY = 'sub';
    const GRANT_TYPE_PAYLOAD_KEY = 'grant_type';
    const EXPIRE_AT_PAYLOAD_KEY = 'exp';
    const JWT_TOKEN_ID = 'jti';
    const SCOPES_PAYLOAD_KEY = 'scopes';
    const ISSUER_AT_PAYLOAD_KEY = 'iat';
    const VALID_FROM_PAYLOAD_KEY = 'nbf';
    const ISSUER_NAME = 'aud';
    const AUTH_NAME = 'iss';
}