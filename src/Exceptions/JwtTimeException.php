<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 12/7/17
 * Time: 9:50 AM
 */

namespace Skipper\JwtAuthClient\Exceptions;

use Skipper\Exceptions\Error;

class JwtTimeException extends AuthClientException
{
    protected function addInstantError(): ?Error
    {
        return new Error('Jwt time exception', 'timeError', 'jwt');
    }
}