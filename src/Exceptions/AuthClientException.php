<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 12/7/17
 * Time: 9:37 AM
 */

namespace Skipper\JwtAuthClient\Exceptions;

use Skipper\Exceptions\DomainException;
use Skipper\Exceptions\Error;

/**
 * Class AuthClientException
 * @package Skipper\JwtAuthClient\Exceptions
 */
class AuthClientException extends DomainException
{
    protected function addInstantError(): ?Error
    {
        return new Error('Jwt error', 'authError', 'jwt');
    }
}