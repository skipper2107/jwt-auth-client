<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 12/7/17
 * Time: 9:48 AM
 */

namespace Skipper\JwtAuthClient\Exceptions;

use Skipper\Exceptions\Error;

class InvalidSignatureException extends AuthClientException
{
    protected function addInstantError(): ?Error
    {
        return new Error('Invalid signature', 'invalidParameter', 'signature');
    }
}