<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 12/7/17
 * Time: 9:44 AM
 */

namespace Skipper\JwtAuthClient\Exceptions;

use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\SignatureInvalidException;

class ExceptionFactory
{
    public static function createFromJwtException(\Exception $exception): \Exception
    {
        if ($exception instanceof \UnexpectedValueException) {
            return new InvalidJwtException($exception->getMessage(), [], $exception, $exception->getCode());
        }

        if ($exception instanceof SignatureInvalidException) {
            return new InvalidSignatureException($exception->getMessage(), [], $exception, $exception->getCode());
        }

        if ($exception instanceof BeforeValidException) {
            return new JwtTimeException('TokenCameTooSoon:' . $exception->getMessage(), [], $exception,
                $exception->getCode());
        }

        if ($exception instanceof ExpiredException) {
            return new JwtTimeException('TokenExpired:' . $exception->getMessage(), [], $exception,
                $exception->getCode());
        }

        return $exception;
    }
}