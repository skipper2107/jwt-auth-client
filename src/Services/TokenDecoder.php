<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 7/28/17
 * Time: 5:33 PM
 */

namespace Skipper\JwtAuthClient\Services;

use Firebase\JWT\JWT;
use Skipper\Exceptions\Error;
use Skipper\JwtAuthClient\Entities\AccessToken;
use Skipper\JwtAuthClient\Entities\GrantType;
use Skipper\JwtAuthClient\Entities\JwtTokenMapper;
use Skipper\JwtAuthClient\Entities\Scope;
use Skipper\JwtAuthClient\Exceptions\AuthClientException;
use Skipper\JwtAuthClient\Exceptions\ExceptionFactory;

class TokenDecoder
{

    /** @var string $publicKeyPath */
    protected $publicKeyPath;
    /** @var array $algorithm */
    protected $algorithm = [];

    /**
     * TokenDecoder constructor.
     * @param string $publicKeyPath
     * @param string $algorithm
     * @throws AuthClientException
     */
    public function __construct(string $publicKeyPath, string $algorithm = JwtTokenMapper::DEFAULT_DECODE_ALGORITHM)
    {
        if (false === is_file($publicKeyPath)) {
            $e = new AuthClientException('Public key does not exists', [
                'file' => $publicKeyPath,
            ]);
            $e->addError(new Error('Invalid public key', 'invalidParameter', 'public_key'));

            throw $e;
        }
        $this->publicKeyPath = $publicKeyPath;
        $this->algorithm = [$algorithm];
    }

    /**
     * @param $stringToken
     * @return AccessToken
     * @throws \Exception
     */
    public function createTokenFromString(string $stringToken): AccessToken
    {
        $tokenObj = $this->decode($stringToken);
        $accessToken = new AccessToken();
        $accessToken->setUserId($tokenObj->{JwtTokenMapper::USER_ID_PAYLOAD_KEY})
            ->setTokenId($tokenObj->{JwtTokenMapper::JWT_TOKEN_ID})
            ->setExpireAt((new \DateTime)->setTimestamp($tokenObj->{JwtTokenMapper::EXPIRE_AT_PAYLOAD_KEY}))
            ->setIssuedAt((new \DateTime)->setTimestamp($tokenObj->{JwtTokenMapper::ISSUER_AT_PAYLOAD_KEY}))
            ->setGrantType(new GrantType($tokenObj->{JwtTokenMapper::GRANT_TYPE_PAYLOAD_KEY}))
            ->setIssuer($tokenObj->{JwtTokenMapper::ISSUER_NAME});
        $scopes = [];
        foreach ($tokenObj->{JwtTokenMapper::SCOPES_PAYLOAD_KEY} as $scopeName => $scopePermissions) {
            $scope = new Scope();
            $scope->setPermissions($scopePermissions);
            $scope->setDomain($scopeName);
            $scopes[] = $scope;
        }
        $accessToken->setScopes($scopes);

        return $accessToken;
    }

    /**
     * @param $stringToken
     * @return \stdClass
     * @throws \Exception
     */
    private function decode(string $stringToken): \stdClass
    {
        $key = openssl_pkey_get_public(file_get_contents($this->publicKeyPath));
        try {
            $object = JWT::decode($stringToken, $key, $this->algorithm);
        } catch (\Exception $exception) {
            throw ExceptionFactory::createFromJwtException($exception);
        }

        return $object;
    }
}